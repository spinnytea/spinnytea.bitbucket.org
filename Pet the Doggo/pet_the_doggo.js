(function() {
	//
	// utils
	//

	function randomElement (array) {
		return array[Math.floor(Math.random()*array.length)];
	}

	// bugfix: prevent double-tap zoom on mobile
	$(window).bind('touchend', function(e) { e.preventDefault(); });

	function debug(str) {
		$('<pre>').text(str || 'undefined').appendTo($('body'));
	}


	//
	// background music
	//

	const baseURL = window.location.href.substring(0, window.location.href.lastIndexOf('/'));
	const bgvolume = 0.8;
	const bgmusic = new Audio(baseURL + '/audio/bgmusic1.mp3');
	let bgmusicShouldBePaused;
	bgmusic.loop = true;
	bgmusic.volume = bgvolume;

	document.addEventListener('visibilitychange', function () {
		if (document.hidden) {
			bgmusicShouldBePaused = bgmusic.paused;
			bgmusic.pause();
		}
		else if(!bgmusicShouldBePaused) bgmusic.play();
	})

	function playScratch () {
		return new Promise(function (resolve) {
			const scratch = new Audio(baseURL + '/audio/scratch.mp3');
			bgmusic.pause();
			scratch.currentTime = 0;
			scratch.play();
			scratch.onended = function () {
				resolve();
			};
		});
	}


	//
	// speech: say('phrase');
	//

	let lastUtterance;
	let volumeTween;
	let tweenUpdateId;

	// initialze/load voices
	speechSynthesis.getVoices();
	// pick any voice from the list
	function getVoice(...names) {
		var vs = speechSynthesis.getVoices();
		let v;
		// TODO check priority based on lower index
		for(let i=0; i<vs.length; i++) {
			if(names.indexOf(vs[i].name) !== -1) {
				v = vs[i];
			}
		}
		return v;
	};

	function tweenLoop () {
		TWEEN.update();
		if(tweenUpdateId) tweenUpdateId = requestAnimationFrame(tweenLoop);
	}
	
	function say (phrase, volume = 0.4) {
		const utterance = lastUtterance = new SpeechSynthesisUtterance();
		utterance.text = phrase;
		const voice = getVoice('Yuri', 'Russian Russia');
		if(voice) {
			utterance.voice = voice;
			utterance.voiceURI = voice.voiceURI;
			utterance.lang = voice.lang;
		}
		if (volumeTween) {
			tweenUpdateId = cancelAnimationFrame(tweenUpdateId);
			volumeTween.stop();
		}
		bgmusic.volume = bgvolume * volume;
		speechSynthesis.speak(utterance);
		utterance.onend = function unduckAudio () {
			setTimeout(() => {
				if (utterance === lastUtterance) {
					let nextState = { volume: bgmusic.volume };
					volumeTween = new TWEEN.Tween(nextState)
						.to({ volume: bgvolume }, 1000)
						.easing(TWEEN.Easing.Linear.None)
						.onUpdate(() => bgmusic.volume = nextState.volume)
						.onComplete(() => tweenUpdateId = cancelAnimationFrame(tweenUpdateId))
						.start();
					tweenUpdateId = requestAnimationFrame(tweenLoop);
				}
			}, 500);
		};
	}


	//
	// doggos
	//

	const doggos = [
		{ image: 'images/doggo1.gif', phrase: 'woke', the: 'the ', name: 'Woofer', clickCount: 0 },
		{ image: 'images/doggo2.gif', phrase: 'woof', the: 'the ', name: 'Pibble', clickCount: 0 },
		{ image: 'images/doggo3.gif', phrase: 'au', the: 'the ', name: 'Pupper', clickCount: 0 },
		{ image: 'images/doggo4.gif', phrase: 'arf', the: 'the ', name: 'Longo Doggo', clickCount: 0 },
		{ image: 'images/doggo5.gif', phrase: 'bub', the: 'the ', name: 'Yapper', clickCount: 0 },
		{ image: 'images/doggo6.gif', phrase: 'whou', the: 'the ', name: 'Howler', clickCount: 0 },
		{ image: 'images/doggo7.gif', phrase: 'bhao', the: 'the ', name: 'Pupperino', clickCount: 0 },
		{ image: 'images/doggo8.gif', phrase: 'haf', the: 'the ', name: 'Waggly Needle', clickCount: 0 },
		{ image: 'images/doggo9.gif', phrase: 'wow', the: 'the ', name: 'Corgo', clickCount: 0 },
	];
	const nina = { image: 'images/nina.gif', phrase: 'ed... ward?', the: '', name: 'Nina', clickCount: 0 };
	ninaTimer = 2;
	let current;

	function nextDoggo (idx) {
		if(idx === undefined) {
			if(ninaTimer < 0) {
				ninaTimer = 3;
				current = nina;
			} else {
				// must pick a new one
				const prev = current;
				while(prev === current) current = randomElement(doggos);
			}
		} else {
			// for debugging
			current = doggos[idx];
		}
		$('#doggo')
			.attr('class', current.name)
			.attr('src', current.image);
	}


	//
	// stats
	//

	const stats = {
		clickCount: 0, // general progression
		binCount: 0, // say 'never too many pets'
	};

	// populate the list of buckets from the ones defined in html
	const buckets = [];
	$('.bucket').each(function() {
		const e = $(this);
		const name = e.attr('id');
		const o = e.offset();

		// create a bucket
		buckets.push({
			name: name,
			bounds: {
				top: o.top,
				left: o.left,
				width: e.width(),
				height: e.height(),
			},
		});

		// say 'so many pets' for each bin
		stats['bucket'+name] = 0;
	});
	// const roof = _.remove(buckets, { name: 'Roof' });
	const roof = buckets.filter((b) => (b.name === 'Roof'))[0];
	buckets.splice(buckets.indexOf(roof), 1);

	function youMonster () {
		say(`You aren't going to pet ${current.the}${current.name}? You monster.`);
		$('#heart').addClass('broke');
		if(calcLove() !== 0) {
			stats.clickCount--;
			renderHeart();
		}
	}

	let monsterId;
	function clickDoggo() {
		// before
		$('#heart').removeClass('broke');
		clearTimeout(monsterId);
		monsterId = setTimeout(()=> {
			bgmusicShouldBePaused = true;
			if (!bgmusic.paused) playScratch().then(youMonster);
			else youMonster();
			ninaTimer--;
		}, 5000);
		if(current.clickCount === 0) {
			say(`You pet ${current.the}${current.name}`);
		}

		// update stats
		stats.clickCount++;
		current.clickCount++;
		
		// after
		const love = calcLove();
		if(love === 0) {
			bin();
			nextDoggo();
		}
		if(love === 9) {
			if(current !== nina) {
				say(current.phrase);
			}
			$('#heart').addClass('final');
	}

		// render
		renderHeart();
	}
	function calcLove () { return stats.clickCount % 10; } // XXX stats.love?

	// init heart size
	renderHeart();
	function renderHeart() {
		$('#heart').css('font-size', (calcLove()*2+10) + 'pt');
	}

	// put a pupper in a bin
	function bin () {
		bgmusic.play();
		$('#heart').removeClass('final');
		stats.binCount++;
		const bucket = randomElement(buckets);
		const id = 'bucket'+bucket.name;
		const count = ++stats[id];

		if(current === nina) {
			say(current.phrase, 0);
		}

		if(count === 4) {
			// TODO unlock bins
			//  - instead of putting all the bins in at once, hold some back
			//  - unlock more locations when we get to this
			//  - e.g. only first floor is open, and then one at a time
			//  - e.g. only 'floors' (table, stairs, attic) are open, then all surfaces unlock
			//  - e.g. only 3 spots, when all spots have 4 open another location, when all spots have 4 open another, etc
			say('So many pets.');
		}
		if(stats.binCount === 40) {
			say('Too many pets? No, never too many pets.');
			buckets.push(roof); // add roof back in
		}

		$('<img class="doggo" />')
			.attr('src', current.image)
			.addClass(current.name)
			.css({
				'position': 'absolute',
				'top': (Math.random() * bucket.bounds.height) + 'px',
				'left': (Math.random() * bucket.bounds.width) + 'px',
				// 'z-index': Math.floor(y), // no need to set this, it's more visually interesting to let them layer
			})
			.appendTo($('#'+bucket.name));

		// if(count > 1) {
		// 	const phrase = `You have ${count} Doggos on the ${name}.`;
		// 	say(phrase);
		// }
	}

	// rando doggo
	// nextDoggo();
	// for(i=0;i<10;i++) {
	// 	bin();
	// 	nextDoggo();
	// }

	// test speach
	// window.say = say;
	
	function introStory () {
		if(speechSynthesis.speaking) return;

		introStory.state++;
		switch(introStory.state) {
			case 0:
				bgmusic.play();
				break;
			case 1:
				bgmusic.play();
				say('In the beginning, there was no Doggo.');
				break;
			case 2:
				$('#heart').removeClass('hidden');
				nextDoggo();
				say(current.phrase);
				break;
			case 3:
				$('body').removeClass('intro');
				$(window).off('mousedown touchstart', introStory);
				$('#doggo').on('mousedown touchstart', clickDoggo);

				say('And then, the Doggos came.');
				monsterId = setTimeout(function() {
					say(`This little ${current.name} would love to be pet.`);
					monsterId = setTimeout(function() {
						$('#heart').addClass('broke');
					}, 10000);
				}, 10000);
				break;
		}
	}
	introStory.state = -1;
	$(window).on('mousedown touchstart', introStory);
})();